/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {

    // make buffer of all nums and their index
    const buf = {};
    nums.forEach((e, i) => buf[e] = i);
    
    for (let i = 0; i < nums.length; i++) {
        const idx = buf[target - nums[i]];
        if(idx && idx != i) {
            return [i, buf[target - nums[i]]]
        }
    }
};
